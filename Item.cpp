#include "Item.h"

Item::Item(string name, string serialNumber, double unitPrice)
{
	this->_count = 1;
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = unitPrice;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return (this->_count * this->_unitPrice);
}

//compares the _serialNumber of those items.
bool Item::operator <(const Item& other) const
{
	return (this->_serialNumber < other._serialNumber);
}

//compares the _serialNumber of those items.
bool Item::operator >(const Item& other) const
{
	return (this->_serialNumber > other._serialNumber);
}

//compares the _serialNumber of those items.
bool Item::operator ==(const Item& other) const
{
	return (this->_serialNumber == other._serialNumber);
}

string Item::getName() const { return this->_name; }
void Item::setName(string name) { this->_name = name; }

string Item::getSerialNumber() const { return this->_serialNumber; }
void Item::setSerialNumber(string serialNumber) { this->_serialNumber = serialNumber; }

int Item::getCount() const { return this->_count; }
void Item::setCount(int count) { this->_count = count; }

double Item::getUnitPrice() const { return this->_unitPrice; }
void Item::setUnitPrice(double unitPrice) { this->_unitPrice = unitPrice; }