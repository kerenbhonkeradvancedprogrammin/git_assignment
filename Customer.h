#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(string name);
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item toAdd);//add item to the set
	void removeItem(Item toRemove);//remove item from the set
	
	void printList();

	string getName();
	void setName(string name);
	set<Item> getItems();
	void setItems(set<Item> newItems);

private:
	string _name;
	set<Item> _items;
};
