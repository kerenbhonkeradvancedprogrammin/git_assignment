#include "Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::~Customer()
{
	this->_items.clear();
}

//returns the total sum for payment
double Customer::totalSum() const
{
	double totalSum = 0;
	set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); it++)
	{
		totalSum += it->totalPrice();
	}

	return totalSum;
}

//add item to the set
void Customer::addItem(Item toAdd)
{
	set<Item>::iterator found = this->_items.find(toAdd);
	Item a("", "", 0);

	if (found != this->_items.end())
	{
		a = *found;
		a.setCount(a.getCount() + 1);
		this->_items.erase(toAdd);
		this->_items.insert(a);
	}
	else
	{
		this->_items.insert(toAdd);
	}
}

//remove item from the set
void Customer::removeItem(Item toRemove)
{
	set<Item>::iterator found;
	Item a("", "", 0);

	if ((found = _items.find(toRemove)) != _items.end())
	{
		a = *found;
		_items.erase(toRemove);
		a.setCount(a.getCount() - 1);
		if (a.getCount() != 0)
		{
			_items.insert(a);
		}
	}
}

void Customer::printList()
{
	set<Item>::iterator it;
	cout << "Your list:" << endl;
	for (it = this->_items.begin(); it != this->_items.end(); it++)
	{
		cout << it->getSerialNumber() << " " << it->getName() << "\tprice: " << it->getUnitPrice() << " count: " << it->getCount() << endl;
	}
}

string Customer::getName() { return this->_name; }
void Customer::setName(string name) { this->_name = name; }

set<Item> Customer::getItems() { return this->_items; }
void Customer::setItems(set<Item> newItems) { this->_items = newItems; }