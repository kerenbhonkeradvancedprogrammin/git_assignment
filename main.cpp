#include"Customer.h"
#include<map>
#include <iostream>
#include <string>
using namespace std;

#define TO_SIGN 1
#define TO_UPDATE 2
#define TO_PRINT 3
#define TO_EXIT 4

#define TO_ADD 1
#define TO_REMOVE 2
#define BACK_TO_MENU 3

#define SIZE 10

int showOptions();
int buyItem(Customer& c, Item itemList[]);
void optionOne(map<string, Customer>& customers, Item itemList[]);
void optionTwo(map<string, Customer>& customers, Item itemList[]);
void optionThree(map<string, Customer> customers);
int removeItem(Customer& c, Item itemList[]);

int main()
{
	int choice;
	map<string, Customer> abcCustomers;
	Item itemList[SIZE] = {
		Item("Milk ","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice ","00006",6.2),
		Item("chicken","00007",25.99),
		Item("fish ", "00008", 31.65),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	do
	{
		choice = showOptions();
		switch (choice)
		{
		case TO_SIGN:
			optionOne(abcCustomers, itemList);
			break;
		case TO_UPDATE:
			optionTwo(abcCustomers, itemList);
			break;
		case TO_PRINT:
			optionThree(abcCustomers);
			break;
		case TO_EXIT:
			break;
		}
	} while (choice != 4);
	system("PAUSE");
	return 0;
}

int showOptions()
{
	int choice;

	cout << endl << "Welcome to MagshiMart!" << endl;
	cout << TO_SIGN << ". to sign as customer and but items" << endl;
	cout << TO_UPDATE << ". to update existing customer's items" << endl;
	cout << TO_PRINT << ". to print the customer who pays the most" << endl;
	cout << TO_EXIT << ". to exit" << endl;

	cout << "Enter your choice: ";
	cin >> choice;
	return choice;
}

int buyItem(Customer& c, Item itemList[])
{
	int choice;

	cout << endl << "The items you can buy are: (0 to exit)" << endl;
	for (int i = 0; i < SIZE; i++)
	{
		cout << (i+1) << ". " << itemList[i].getName() << "\tprice: " << itemList[i].getUnitPrice() << endl;
	}
	cout << "What item would you like to buy? input: ";
	cin >> choice;

	if (choice <= SIZE && choice >= 1)
	{
		c.addItem(itemList[choice-1]);
		cout << endl << itemList[choice-1].getName() << " is added to your shopping list" << endl;
	}
	else if(choice != 0)
	{
		cout << endl << "Error! wrong input!" << endl;
	}
	else
	{
		cout << endl << "shopping ended!" << endl;
	}

	return choice;
}

int removeItem(Customer& c, Item itemList[])
{
	int choice = 0;
	string serialNumber;
	bool found = false;

	cout << endl << "what item do you want to remove? (if the item's serial number is 000001 enter 1, 0 to exit)" << endl;
	c.printList();
	cin >> choice;
	
	set<Item> items = c.getItems();
	set<Item>::iterator it;

	for (it = items.begin(); it != items.end() && found == false; it++)
	{
		if (it->operator==(itemList[choice-1]))
		{
			found = true;
		}
	}

	if (found)
	{
		it--;
		Item itemToRemove(it->getName(), it->getSerialNumber(), it->getUnitPrice());
		c.removeItem(itemToRemove);
	}
	else if (choice != 0)
	{
		cout << endl << "Error! wrong input!" << endl;
	}

	return choice;
}

void optionOne(map<string, Customer>& customers, Item itemList[])
{
	string name;
	int result;

	cout << "Enter your name: ";
	cin >> name;
	map<string, Customer>::iterator it = customers.find(name);
	
	if (it != customers.end())
	{
		cout << "The customer is already exists" << endl;
	}
	else
	{
		Customer newCustomer(name);
		do
		{
			result = buyItem(newCustomer, itemList);
		} while (result != 0);
		customers.insert(pair<string, Customer>(name, newCustomer));
	}

}

void optionTwo(map<string, Customer>& customers, Item itemList[])
{
	string name;
	int option, result;

	cout << "Enter your name: ";
	cin >> name;
	map<string, Customer>::iterator it = customers.find(name);
	
	if (it == customers.end())
	{
		cout << "The customer isn't exists" << endl;
	}
	else
	{
		//Customer c = it->second;
		it->second.printList();

		cout << "what do you want to do?" << endl;
		cout << TO_ADD << ". Add items" << endl;
		cout << TO_REMOVE << ". Remove items" << endl;
		cout << BACK_TO_MENU << ". Back to menu" << endl;
		cin >> option;

		switch (option)
		{
		case TO_ADD:
			do
			{
				result = buyItem(it->second, itemList);
			} while (result != 0);
			break;
		case TO_REMOVE:
			do
			{
				result = removeItem(it->second, itemList);
			} while (result != 0);

			break;
		case BACK_TO_MENU:
			break;
		default:
			cout << endl << "Error! wrong input!" << endl;
		}

	}

}

void optionThree(map<string, Customer> customers)
{
	map<string, Customer>::iterator biggest = customers.begin();
	map<string, Customer>::iterator it;

	for (it = customers.begin(); it != customers.end(); it++)
	{
		if (it->second.totalSum() > biggest->second.totalSum())
		{
			biggest = it;
		}
	}

	cout << endl << "The customer who paid the most is " << biggest->first << "\tthe total price is: " << biggest->second.totalSum() << endl;
}